import Vue from 'vue'
import Vuex from 'vuex'
// import createPersistedState from 'vuex-persistedstate'


////////////////////////////////////////

//Modules
// import counter from './modules/counter';
// import session from './modules/session';
import ui from './modules/ui';


//Initiate Vuex
Vue.use(Vuex)

////////////////////////////////////////

//Create the store
const store = new Vuex.Store({
    modules: {
        ui,
    },
    state: {
        email: '',
        definitions: null,
    },
    mutations: {
        email(state, payload) {
            state.email = payload;
        },
        definitions(state, payload) {
            state.definitions = payload;
        },
    },
    getters: {
        email(state, getters) {
            return state.email;
        },
        definitions(state, getters) {
            return state.definitions;
        },
    },
})

///////////////////////////////////////
//Add to the root
Vue.$store = store;


////////////////////////////////////////

//Export it
export default store;